<?php
/**
 * Created by PhpStorm.
 * User: sawyerlancer
 * Date: 26.04.18
 * Time: 2:39
 */ ?>


<div class="container" style="margin-bottom: 100px;">


    <div class="jumbotron" style="background: none;" id="company">
        <h2 class="text-center ">
            О компании
        </h2>

        <br>

        <p class="text-center">
            Наша Группа компаний образована и работает с 2002 года. Мы занимаемся международной торговлей.

            Компания «T&M CARGO» - специально созданная транспортно-логистическая компания внутри нашей Группы.
            Она оказывает услуги по перевозке грузов из Европы, США, Канады для сторонних организаций и для собственных нужд.
        </p>

    </div>

    <div class="row" id="servies">

        <div class="col-lg-4">
            <div class="card" style="width:auto; border: none;">
                <div class="mx-auto">
           <img class="card-img-top wow bounceInUp" src="<?php echo get_template_directory_uri(); ?>/img/delivery.svg" alt="Card image" style="width: 120px;">
                </div>
                <div class="card-body wow bounceInUp">
                    <h5 class="card-title text-center">Транспортные услуги и логистика</h5>


                </div>

        </div>
        </div>


        <div class="col-lg-4">
            <div class="card" style="width:auto; border: none;">
                <div class="mx-auto">
                    <img class="card-img-top wow bounceInUp" src="<?php echo get_template_directory_uri(); ?>/img/delivery-box.svg" alt="Card image" style="width: 120px;">
                </div>
                <div class="card-body wow bounceInUp">
                    <h5 class="card-title text-center">Аутсорсинг услуг таможенного посредничества</h5>


                </div>

            </div>
        </div>


        <div class="col-lg-4">
            <div class="card" style="width:auto; border: none;">
                <div class="mx-auto">
                    <img class="card-img-top wow bounceInUp" src="<?php echo get_template_directory_uri(); ?>/img/money.svg" alt="Card image" style="width: 120px;">
                </div>
                <div class="card-body wow bounceInUp">
                    <h5 class="card-title text-center">Аутсорсинг бухгалтерских услуг</h5>


                </div>

            </div>
        </div>

    </div>


        <div class="row" style="margin:100px 0">
        <div class="col-lg-6 wow bounceInLeft" >
        <img src="<?php echo get_template_directory_uri(); ?>/img/first-slide.jpeg" class="rounded" width="100%" >

        </div>
            <div class="col-lg-6 wow bounceInRight" style="padding-top: 25px;">
                <h4 class="text-center"  style="margin-bottom: 25px;">Транспортные услуги и логистика</h4>


                <div class="bxslider" style="height: 600px;">

                    <div>
                        <ul>
                            <li>
                                перевозке консолидированных, мелких-частичных, крупных и нестандартных грузов
                            </li>
                            <li>
                                перевозке опасных грузов
                            </li>
                            <li>
                                транспортировке личных, дипломатических и других ценных грузов
                            </li>
                            <li>
                                услуги Складского комплекса на территории г. Вильнюс (Литва)
                            </li>
                        </ul>
                    </div>

                    <div>
                        Мы ответственно выполняем не только перевозку, но и перегрузку товара. Стремясь обеспечить безопасную доставку груза, используем меры предосторожности (специальные укрепления, воздушные подушки).

                    </div>

                    <div>
                        В транспортные услуги входят мультимодальные перевозки - грузоперевозка сухопутным, железнодорожным, морским и воздушным транспортом.


                    </div>
                </div>



            </div>
        </div>

    <div class="row" style="margin:100px 0">
        <div class="col-lg-6 wow bounceInLeft">
            <h4 class="text-center "  style="margin-bottom: 25px;">Аутсорсинг ВЭД </h4>

            <div class="bxslider">
            <div>
                <ul>
                    <li>
                        Передача функций по доставке и таможенному оформлению груза
                    </li>
                    <li>
                        Передача функции импортера
                    </li>
                    <li>
                        транспортировке личных, дипломатических и других ценных грузов
                    </li>
                    <li>
                        услуги Складского комплекса на территории г. Вильнюс (Литва)
                    </li>
                </ul>
            </div>
                <div>

                    Учитывая наши возможности по организации транспортно-логистических процессов и таможенному оформлению товаров в Европе, Америке,
                    Канаде на самых первых шагах развития компании мы поняли востребованность получения всех услуг, связанных с организацией внешнеторговых поставок, по принципу «одно окно».
                    ВЭД-оператор – это Ваше «одно окно» в сложной системе коммерческих отношений, возникающих при поставках товаров из-за рубежа.
                    «T&M CARGO» готова взять на себя как полный комплекс, так и часть мероприятий, связанных с реализацией внешнеэкономической сделки.
                </div>
    <div>
        Мероприятия, реализуемые «T&M CARGO», в рамках данной схемы:

        <ul>
            <li>
                подготовка внешнеэкономического контракта поставки
            </li>
            <li>
                выбор оптимальной логистической схемы
            </li>
            <li>
                расчет стоимости поставки и согласование графика платежей
            </li>
            <li>
                проведение оплат
            </li>
            <li>
                согласование отгрузки с поставщиком
            </li>
            <li>
                организация перевозки груза
            </li>
            <li>
                подготовка необходимой разрешительной документации (при необходимости)
            </li>
            <li>
                организация таможенного оформления
            </li>
            <li>
                доставка груза по указанному адресу
            </li>
            <li>
                передача товара со всеми необходимыми документами
            </li>
        </ul>
    </div>
            </div>

        </div>
        <div class="col-lg-6 wow bounceInRight" style="padding-top: 25px;">
            <img src="<?php echo get_template_directory_uri(); ?>/img/two-slide.jpg" class="rounded" width="100%" >

        </div>
    </div>


    <div class="row" style="margin:100px 0">
        <div class="col-lg-6 wow bounceInLeft" >
            <img src="<?php echo get_template_directory_uri(); ?>/img/three-slide.jpeg" class="rounded" width="100%" >

        </div>
        <div class="col-lg-6 wow bounceInRight" style="padding-top: 25px;">
            <h4 class="text-center"  style="margin-bottom: 25px;">Услуги по бухгалтерскому учету</h4>





            <div class="bxslider">
            <div>
                «T&M CARGO» может полностью взять на себя Ваш бухгалтерский учет, либо позаботиться о его отдельных участках – от сбора и проверки первичной документации до формирования и сдачи ответов, сопровождения проверок со стороны государственных органов и др.
                Мы работаем с организациями, имеющими разные организационно-правовые формы.
            </div>
            <div>
                Наш многолетний опыт, применение передовых технологий и практик позволяют нам выполнять бухгалтерские задачи любой сложности в компаниях разного масштаба.
                Мы гарантируем корректность бухгалтерского учета, своевременность сдачи обязательной отчетности в полном соответствии законодательству Республики Казахстан
                соблюдение строгих правил соблюдения конфиденциальности при работе с данными Клиента.
                Наши услуги покрываются страховкой профессиональной ответственности, что дает нашим Клиентам дополнительные гарантии защиты.
                Мы может вести Вашу бухгалтерию полностью либо взять на себя её отдельные участки.
            </div>
            <div>
                Стандартный пакет услуг включает в себя:

                <ul>
                    <li>
                        Отражение операций на счетах бухгалтерского и налогового учета
                    </li>
                    <li>
                        Ведение и учет банковских и кассовых операций
                    </li>
                    <li>
                        Учет основных средств
                    </li>
                    <li>
                        Обработка авансовых отчетов
                    </li>
                    <li>
                        Расчеты с поставщиками и Клиентами
                    </li>
                    <li>
                        Проверка и восстановление бухгалтерского учета
                    </li>
                    <li>
                        Закрытие отчетных периодов
                    </li>
                    <li>
                        Подготовка и представление в государственные органы всей обязательной отчетности и деклараций
                    </li>
                    <li>
                        Подготовка управленческой и международной отчетности
                    </li>
                </ul>

            </div>
        </div>

        </div>
    </div>



    <h1 class="text-center" id="request">Запрос транспортировки</h1>

    <form id="form" method="post" action="" id="validation">
        <div class="row" >
            <div class="col-lg-12"> <p>КОНТАКТНАЯ ИНФОРМАЦИЯ</p> </div>
        </div>
    <div class="row">

        <div class="col-lg-6">

            <div class="margin-bottom-field">

                <img src="<?php echo get_template_directory_uri(); ?>/img/issue-opened.svg"  width="20px"  height="20px">
                <label  for="inputCompanyName"> Название компании:</label>

                <input type="text" name="nameCompany" class="form-control" required="required"  id="inputCompanyName" >


            </div>

            <div class="margin-bottom-field">
                <img src="<?php echo get_template_directory_uri(); ?>/img/issue-opened.svg"  width="20px"  height="20px">
                <label  for="inputEmail">Email:</label>

            <input type="email" name="email" class="form-control" required="required"  id="inputEmail" >


            </div>

        </div>

      <div class="col-lg-6">

          <div class="margin-bottom-field">
              <img src="<?php echo get_template_directory_uri(); ?>/img/issue-opened.svg"  width="20px"  height="20px">
              <label for="name">Ф.И.О.:</label>

              <input type="text" name="name" class="form-control" required="required" id="name">
          </div>

          <div class="margin-bottom-field">
              <img src="<?php echo get_template_directory_uri(); ?>/img/issue-opened.svg"  width="20px"  height="20px">
              <label for="phone">Телефон:</label>

              <input type="text" name="phone" class="form-control" required="required" id="phone">
          </div>

      </div>

        </div>

        <div class="row">
            <div class="col-lg-12"> <p>ИНФОРМАЦИЯ О ПОГРУЗКЕ/ВЫГРУЗКЕ</p> </div>
        </div>


        <div class="row">
            <div class="col-lg-6 margin-bottom-field"> <label for="country&city&load">Место погрузки:</label>

                <input type="text" name="country&city&load" class="form-control" placeholder="Страна,Название или код города"  id="country&city&load">

            </div>

            <div class="col-lg-6 margin-bottom-field"> <label for="country&city&unload">Место выгрузки:</label>

                <input type="text" name="country&city&unload" class="form-control" placeholder="Страна,Название или код города" id="country&city&unload">

            </div>

        </div>

        <div class="row">
            <div class="col-lg-6 margin-bottom-field"> <label for="dateload">Дата загрузки:</label>

                <div class="input-group date">
                    <input type="text" class="form-control pickerdate" id="dateload" name="dateload" placeholder="ММ/ДД/ГГГГ">
                </div>

            </div>


            <div class="col-lg-6 margin-bottom-field"> <label for="dateunload">Дата выгрузки:</label>

                <div class="input-group date">
                    <input type="text" class="form-control pickerdate" id="date" name="dateunload" placeholder="ММ/ДД/ГГГГ">
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-6 margin-bottom-field"> <label for="description">Описание груза:</label>

                <input type="text" name="description" class="form-control"   id="description">

            </div>

            <div class="col-lg-6 margin-bottom-field"> <label for="code">Код ТНВЭД:</label>

                <input type="text" name="code" class="form-control"   id="code">

            </div>

        </div>

        <div class="row">
            <div class="col-lg-4 margin-bottom-field"> <label for="weightnumber">Вес:</label>

                <input type="text" name="weightnumber" class="form-control"  id="weightnumber">

            </div>

            <div class="col-lg-4 margin-bottom-field"> <label for="unit">Единица измерения:</label>

                <select class="custom-select" name="unit" id="unit">
                    <option selected>Выбирите единицу измерения</option>
                    <option value="кг">в килограммах</option>
                    <option value="т">в тоннах</option>
                </select>

            </div>
            <div class="col-lg-4 margin-bottom-field"> <label for="weight">Масса товара:</label>

                <select class="custom-select" name="weight" id="weight">
                    <option selected>Укажите массу товара</option>
                    <option value="брутто">Брутто</option>
                    <option value="нетто">Нетто</option>
                </select>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12"> <p>ИНФОРМАЦИЯ О ГРУЗЕ</p> </div>
        </div>

        <div class="row">
            <div class="col-lg-4 margin-bottom-field">
            <p>Количество и размеры груза:</p>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="colvocargo" value="Европаллеты" name="colvocargo" class="custom-control-input">
                <label class="custom-control-label" for="colvocargo">Европаллеты</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="colvocargoanother"  value="Другая упаковка" name="colvocargo" class="custom-control-input">
                <label class="custom-control-label" for="colvocargoanother">Другая упаковка</label>
            </div>
            </div>

            <div class="col-lg-4 margin-bottom-field">
                <p>Опасный груз:</p>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="dangerousyes" value="да" name="dangerous" class="custom-control-input">
                    <label class="custom-control-label" for="dangerousyes">Да</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="dangerousno" value="нет" name="dangerous" class="custom-control-input">
                    <label class="custom-control-label" for="dangerousno">Нет</label>
                </div>


            </div>

            <div class="col-lg-4 margin-bottom-field">
                <label for="weightnumber">Класс АДР:</label>
                <input type="text" name="dangerouslevel" class="form-control"  id="dangerouslevel">
            </div>

        </div>

        <div class="row">

            <div class="col-lg-4 margin-bottom-field">
                <p>Нужна постоянная температура:</p>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="temperatureyes" value="да" name="temperature" class="custom-control-input">
                    <label class="custom-control-label" for="temperatureyes">Да</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="temperatureno" value="нет" name="temperature" class="custom-control-input">
                    <label class="custom-control-label" for="temperatureno">Нет</label>
                </div>

            </div>

            <div class="col-lg-1 margin-bottom-field">
                <label for="temperatureC">C°:</label>
                <input type="text" name="temperatureC°" class="form-control"  id="temperatureC">
            </div>

            <div class="col-lg-1 margin-bottom-field">
                <label for="colvo">Ед.:</label>
                <input type="text" name="colvo°" class="form-control"  id="colvo">

            </div>

            <div class="col-lg-1 margin-bottom-field">
                <label for="height">Высота:</label>
                <input type="text" name="height°" class="form-control"  id="height">
            </div>

            <div class="col-lg-1 margin-bottom-field">
                <label for="width">Ширина:</label>
                <input type="text" name="width°" class="form-control"  id="width">
            </div>

            <div class="col-lg-1 margin-bottom-field">
                <label for="lenght">Длина:</label>
                <input type="text" name="lenght°" class="form-control"  id="lenght">
            </div>


            <div class="col-lg-2 margin-bottom-field">
                <label for="weight_new">Единица измерения:</label>
                <select class="custom-select " name="weight_new" id="weight_new">
                    <option  value="см" selected>см</option>
                    <option value="мм">мм</option>
                    <option value="м">м</option>
                </select>
            </div>

        </div>


        <div class="row">

            <div class="col-lg-12 margin-bottom-field" >

                <div class="col-lg-12 margin-bottom-field" >

                    <label for="comment">Комментарий:</label>
                    <textarea name="comment" class="form-control" rows="5"  id="comment"></textarea>
                </div>
        </div>


        </div>


        <div class="row">
        <div class="col-lg-2" >

            <button type="submit" style="width: 100%;" class="btn btn-primary">Отправить</button>
        </div>
        </div>

    </form>





</div>


