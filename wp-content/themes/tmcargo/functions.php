<?php
/**
 * Created by PhpStorm.
 * User: sawyerlancer
 * Date: 25.04.18
 * Time: 1:54
 */

function load_bootstrap()
{
   // wp_enqueue_script("jquery-js",get_template_directory_uri().'/js/jquery.min.js');



    wp_enqueue_script(" jquery-1.9.1.min.js",get_template_directory_uri().'/js/jquery-1.9.1.min.js');

    wp_enqueue_script("bootstrap-js",get_template_directory_uri().'/js/bootstrap.js');

    wp_enqueue_script("bootstrap-datepicker-js",get_template_directory_uri().'/js/bootstrap-datepicker.min.js');

    wp_enqueue_script("popper-js",get_template_directory_uri().'/js/popper.min.js');

    wp_enqueue_script("popper-js",get_template_directory_uri().'/js/popper.min.js');

    wp_enqueue_script("wow-js",get_template_directory_uri().'/js/wow.js');

    wp_enqueue_script("slider-js",get_template_directory_uri().'/js/jquery.bxslider.js');

    wp_enqueue_script("svd-js",get_template_directory_uri().'/js/svd.js');



    wp_enqueue_script("bootstrap-datepicker-locale-ru-js",get_template_directory_uri().'/js/locales/bootstrap-datepicker.ru.min.js');

    wp_enqueue_style("bootstrap-css",get_template_directory_uri().'/css/bootstrap.css');
    //wp_enqueue_style("bootstrap3-css",get_template_directory_uri().'/css/bootstrap-3.min.css');
    wp_enqueue_style("bootstrap-grid-css",get_template_directory_uri().'/css/bootstrap-grid.css');
    wp_enqueue_style("bootstrap-reboot-css",get_template_directory_uri().'/css/bootstrap-reboot.css');
    wp_enqueue_style("style-css",get_template_directory_uri().'/css/style.css');
    wp_enqueue_style("animate-css",get_template_directory_uri().'/css/animate.css');
    wp_enqueue_style("slider-css",get_template_directory_uri().'/css/jquery.bxslider.css');
    wp_enqueue_style("font-awesome-css",get_template_directory_uri().'/css/font-awesome.min.css');
    wp_enqueue_style("bootstrap-datepicker-css",get_template_directory_uri().'/css/bootstrap-datepicker.css');
    wp_enqueue_style("bootstrap-datepicker3-css",get_template_directory_uri().'/css/bootstrap-datepicker3.css');


}

function wploop_email_content_type()
{
    return 'text/html';
}

function wploop_email_charset()
{
    return 'UTF-8';
}



add_filter('wp_mail_charset','wploop_email_charset');
add_filter('wp_mail_content_type','wploop_email_content_type');

function sendEmail($nameCompany,$email,$name,$phone,
                   $countrycityload,$countrycityunload,$dateload,
                   $dateunload,$description,$code,$weightnumber,$unit,
                   $weight,$colvotargo,$dangerous,$dangerousLevel,$temperature,
                   $temperatureC°,$colvo,$height,$width,$lenght,$weight_new,$comment,
                   $emailDefault='soldier707sawyer@gmail.com')
{

    $to = $emailDefault;
    $subject = 'письмо от клиента';
    $body=
        "
<html>
<head>
</head>
<body>
<h1 style='text-align: center'>Письмо</h1>
<h2 style='text-align: center'>от: ".$name."</h2>
<br>
<h4 style='text-align: left;'>КОНТАКТНАЯ ИНФОРМАЦИЯ</h4>
<p style='text-align: left;'>Название компании: ".$nameCompany."</p>
<p style='text-align: left;'>Email: ".$email."</p>
<p style='text-align: left;'>Телефон: ".$phone."</p>
<h2 style='text-align: left;'>ИНФОРМАЦИЯ О ПОГРУЗКЕ/ВЫГРУЗКЕ</h2>
<p style='text-align: left;'>Место погрузки: ".$countrycityload."</p>
<p style='text-align: left;'>Место выгрузки: ".$countrycityunload."</p>
<p style='text-align: left;'>Дата загрузки: ".$dateload."</p>
<p style='text-align: left;'>Дата выгрузки: ".$dateunload."</p>
<p style='text-align: left;'>Описание груза: ".$description."</p>
<p style='text-align: left;'>Код ТНВЭД: ".$code."</p>
<p style='text-align: left;'>Вес: ".$weightnumber." ".$unit." ".$weight."</p>
<h2 style='text-align: left;'>ИНФОРМАЦИЯ О ГРУЗЕ</h2>
<p style='text-align: left;'>Количество и размеры груза: ".$colvotargo."</p>
<p style='text-align: left;'>Опасный груз: ".$dangerous."</p>
<p style='text-align: left;'>Класс АДР: ".$dangerousLevel."</p>
<p style='text-align: left;'>Нужна постоянная температура: ".$temperature." ".$temperatureC°."</p>
<p style='text-align: left;'>Количество товара: ".$colvo."</p>
<p style='text-align: left;'>Габариты товара: высота: ".$height.$weight_new." ширина: ".$width.$weight_new." длина: ".$lenght.$weight_new."</p>
<p style='text-align: left;'>Комментарий: ".$comment."</p>
<br>

</body>
</html>
       ";

    if(wp_mail($to,$subject,$body))
    {
        return true;
    }
    else
        {
            return false;
        }

}



add_action('wp_enqueue_scripts','load_bootstrap');


?>